#include <iostream>
#include <stdlib.h>
#include <sstream>
#include <time.h>
#include <stdio.h>
#include <X11/Xlib.h>
#include <pthread.h>
#include <math.h>

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/nonfree/nonfree.hpp"
#include "opencv2/nonfree/features2d.hpp"
#include "analyseProjecteur.hpp"

#ifndef _CRT_SECURE_NO_WARNINGS
# define _CRT_SECURE_NO_WARNINGS
#endif

using namespace cv;
using namespace std;


/* 
 * Déclaration des variables globale * 
 */


/*
 * Classe contenant les paramètres d'une caméra
 * */
	
	void Camera::read(const FileNode& node){

        node["Numero_Camera" ] >> cameraID;
		printf("Affichage du numéro de la caméra : %d\n", cameraID);
        node["Camera_Matrix" ] >> cameraMatrix;
        node["Distortion_Coefficients" ] >> distcoefs;
		node["Rotation_Vector"] >> rvecs;
		node["Translation_Vector"] >> tvecs;
		printf("Activation de la camera %d\n", cameraID+1);
		activation();
		printf("La camera %d %s activée\n\n", cameraID+1, goodInput ? "s'est correctement" : "ne s'est pas correctement");
	}

	void Camera::write(FileStorage& fs){

		fs << "{" << "Numero_Camera" << cameraID
				  << "Camera_Matrix" << cameraMatrix
				  << "Distorsion_Coefficients" << distcoefs
				  << "Rotation_Vector" << rvecs
				  << "Translation_Vector" << tvecs
			<< "}";
	}

	void Camera::activation(){

		goodInput = true;

		if(cameraID < 0 || cameraID > 2){
			
			goodInput = false;

			printf("Valeur incorrecte pour l'ID de la caméra : %d\n", cameraID+1);
		}
		else {
				if(inputCapture.open(cameraID)){
					printf("Caméra N°%d ouverte\n", cameraID+1);
					//inputCapture.set(CV_CAP_PROP_FRAME_WIDTH, 640);
					//inputCapture.set(CV_CAP_PROP_FRAME_HEIGHT, 480);
				}
				else{
					printf("Camera N°%d n'a pas été ouverte\n", cameraID+1);
					if(cameraID < 2)
						goodInput = false;
				}
		}

		if(cameraMatrix.rows !=3 || cameraMatrix.cols !=3){
			
			goodInput = false;
			printf("La matrice de paramètre instrinsèque n'a pas les bonnes dimensions : %dx%d\n", cameraMatrix.rows, cameraMatrix.cols);
		}
		
		if(rvecs.rows != 3 || rvecs.cols != 3){

			goodInput = false;
			printf("Le vecteur de rotation n'a pas les bonnes dimensions : %dx%d\n", rvecs.rows, rvecs.cols);
		}
		
		if(tvecs.rows != 3 || tvecs.cols != 1){

			goodInput = false;
			printf("Le vecteur de translation n'a pas les bonnes dimensions : %dx%d\n", tvecs.rows, tvecs.cols);
		}


		/*outputFile = "params";
		stringstream ss;
		ss << cameraID;
		string str = ss.str();
		outputFile += str;
		outputFile += ".yml";
		rvecs.create(3, 3, CV_32F);
		tvecs.create(3, 3, CV_32F);*/
	}

	Mat Camera::image(){
		
		Mat result;
        if( inputCapture.isOpened() ){
			
			Mat view0;
            inputCapture >> view0;
            view0.copyTo(result);
		}
		return result;
	}


/*
 * Signatures des fonctions
 */
void usage();
bool mire(Mat& img, Size boardSize, vector<Point2f>& buf);
Mat surfMethode(Mat img, Mat pict);
Mat orbMethode(Mat img, Mat pict);

/*
 * Fonction principale prenant un, deux ou trois argumments, c'est à dire les fichiers contenant les paramètres instrinsèques de la ou des caméras.
 * */
int main(int argc, char* argv[]){

	printf("Programme de lecture des images projetées par les vidéo-projecteurs\n");
	printf("Les fichiers contenant les paramètres intrinsèques de la caméra 1 (et de la 2 si existante) sont passés en paramètres du programme\n\n");
	
	if(argc < 2){
		
		usage();
		return EXIT_FAILURE;
	}

	/*
	 * Déclaration et initialisation des variables du programme
	 */
	pthread_t thread_s;
	Camera c1, c2, c3, actuelle;
	const string inputFile1 = argc > 1? argv[1] : "";
	const string inputFile2 = argc > 2? argv[2] : "";
	const string inputFile3 = argc > 3? argv[3] : "";

	/*
	 * Ouverture du premier fichier de paramètre
	 * */
	FileStorage fs(inputFile1, FileStorage::READ);
	if (!fs.isOpened()){
        cout << "Echec de l'ouverture du fichier de paramètre : \"" << inputFile1 << "\"" << endl;
        return EXIT_FAILURE;
    }
	else {
		cout << "Nom du fichier ouvert : " << inputFile1 << endl;
		c1.read(fs["Settings"]);
		fs.release();
		if (!c1.goodInput){
			cout << "Erreur lors de la lecture de :\"" << inputFile1 <<"\". Arret du programme." << endl;
			return EXIT_FAILURE;
			}
	}
	/*
	 * Ouverture du second fichier de paramètre
	 * */
	if(argc > 2)
		fs.open(inputFile2, FileStorage::READ); // Lecture des paramètres de la seconde caméra
	if (!fs.isOpened() && argc > 2){
        
		cout << "Echec de l'ouverture du fichier de paramètre: \"" << inputFile2 << "\"" << endl;
        return EXIT_FAILURE;
    }else {
		cout << "Nom du fichier ouvert : " << inputFile2 << endl;
    	c2.read(fs["Settings"]);
    	fs.release();
		if (!c2.goodInput){
        
			cout << "Erreur lors de la lecture de : \"" << inputFile2 <<"\". Arrêt du programme." << endl;
        	return EXIT_FAILURE;
		}
    }
	/*
	 * Ouverture du troisième fichier de paramètre
	 * */
	if(argc > 3){
		fs.open(inputFile3, FileStorage::READ); // Lecture des paramètres de la troisième caméra
		if (!fs.isOpened()){
			
			cout << "Echec de l'ouverture du fichier de paramètre: \"" << inputFile3 << "\"" << endl;
			return EXIT_FAILURE;
		}
		else{
			c3.read(fs["Settings"]);
			fs.release();
			
			if (!c3.goodInput){
			
				cout << "Erreur lors de la lecture de : \"" << inputFile3 <<"\". Arrêt du programme." << endl;
				return EXIT_FAILURE;
			}
		}
	}



	/*
	 * Déclaration et initialisation des variables d'affichage et de traitement, qui ne sont nécessaires que si toutes les caméras sont correctement ouvertes
	 * *//*
	const char ESC_KEY = 27;
	string fenetre;
	int i, j;
	Size boardSize;
	boardSize.height = 4,
	boardSize.width = 7;
	clock_t prevTimestamp = 0;		
	char key = 'a';
	int nbCam = argc -1;
	Mat img, pict, deform[2];
	vector<Mat> hmgy, aff;
	pict = imread("mire.png", 1);
	vector<Point2f> ptsMire;

	if(mire(pict, boardSize, ptsMire)){
		printf("Points de comparaison trouvés\n");
	}
	else{
		printf("Erreur : impossibilité de trouver correctement le damier dans la mire\n");
		return EXIT_FAILURE;
	}
	
	for(i= 0; i < nbCam ; i ++){
	
		int mode = 0;
		key = 'a';
		bool found;
		vector<Point2f> pointCam;
		destroyAllWindows();


		switch(i){
			case 0 :
				fenetre = "Image Caméra 1";
				actuelle = c1;
				namedWindow(fenetre);
				break;
			
			case 1 :
				fenetre = "Image Caméra 2";
				actuelle = c2;	
				namedWindow(fenetre);
				break;
			case 2 :
				if(argc >= 4){
					Camera * c = &c3;
					/*if (pthread_create(&thread_s, NULL, calibrStream, (void *)c)) {
						
						printf("pthread_create échoué.\n");
					}*//*
				}
				pthread_cancel(thread_s);
				pthread_join(thread_s, NULL);
				break;
		}	
		
		while(key != 'q' && key != ESC_KEY && i < 2){
			
			found = false;
			img = actuelle.image();

			if(img.empty()){
				
				printf("Image vide pour la caméra %d.\n", i+1);
				break;
			}

			if(mode == 1)
				found = mire(img, boardSize, pointCam);
			drawChessboardCorners(img, boardSize, Mat(pointCam), found);
			
			imshow(fenetre, img);

			key = (char)waitKey(50);

			switch(key){
				 case 'c' :
				 	printf("%s", mode == 0? "Capture on\n": "");
					mode = 1;
					break;

				case 'u' :
					printf("%s", mode == 1 ? "Capture off\n": "");
					mode = 0;
					break;
			}
			if(found)
				break;
		}

		if(found){
			/*
			 * Traitement et comparaison des images
			 *//*
			printf("Mire trouvée, transformation des points 2D en points 3D\n\n");
			vector<Mat> pointsWorld, pointImage;
			double k;
			Mat position = -1 * (actuelle.rvecs.t() * actuelle.tvecs);
			
			for(j = 0; j < pointCam.size(); j++){

				pointsWorld.push_back(Mat(3, 1, CV_64F));
				
				pointsWorld[j].at<double>(0,0) =  (pointCam[j].x - actuelle.cameraMatrix.at<double>(0,2))/actuelle.cameraMatrix.at<double>(0,0);
				pointsWorld[j].at<double>(1,0) = (pointCam[j].y - actuelle.cameraMatrix.at<double>(1,2))/actuelle.cameraMatrix.at<double>(1,1);	
				pointsWorld[j].at<double>(2,0) = 1;
				

				/*
				 * Normalisation
				 *//*
				
				float norm = (sqrt(pow(pointsWorld[j].at<double>(0,0), 2.0) + pow(pointsWorld[j].at<double>(1,0), 2.0) + pow(pointsWorld[j].at<double>(2,0), 2.0)));
				pointsWorld[j] = (1.0/norm) * pointsWorld[j];
				
				/*
				 * Multiplication par le vecteur de rotation
				 *//*
				pointsWorld[j] = actuelle.rvecs.t() * pointsWorld[j];

				k = -1.0 * position.at<double>(2,0)/pointsWorld[j].at<double>(2,0);
				
				pointsWorld[j].at<double>(0,0) = position.at<double>(0,0) + k * pointsWorld[j].at<double>(0,0);
				pointsWorld[j].at<double>(1,0) = position.at<double>(1,0) + k * pointsWorld[j].at<double>(1,0);
				pointsWorld[j].at<double>(2,0) = position.at<double>(2,0) + k * pointsWorld[j].at<double>(2,0);
				
				if(pointsWorld[j].at<double>(2,0) != 0){
					printf("Erreur de calcul : zP n'est pas égal à zéro, mais à : %f. k = %f, et zC = %f\n", pointsWorld[j].at<double>(2, 0), k, position.at<double>(2,0));
					return EXIT_FAILURE;
				}

			}
			printf("Points transformés en points 3D\n");
			
			/*
			 * Récupération des transformations image -> projetction
			 *//*
			vector<Point2f> ptTemp, tmp, tmpMire;

			for(j = 0; j < ptsMire.size(); j+=6){
				//tmp.push_back(Point2f(pointsWorld[0].at<double>(0,0), pointsWorld[0].at<double>(1,0)));
				Mat pI = Mat ( pointsWorld[0 ] );
				pointImage.push_back(pI);
				tmp.push_back(Point2f(pointsWorld[j].at<double>(0,0), pointsWorld[j].at<double>(1,0)));
				ptTemp.push_back(pointCam[j]);
				//ptTemp.push_back(Point2f(pointsWorld[0].at<double>(0,0), pointsWorld[0].at<double>(1,0)));
				tmpMire.push_back(ptsMire[j]);
				if(j == 6)
					j = 15;
			}
			//cout << "tmp : " << endl << tmp << endl;
			//cout << "tmp2 : " << endl << ptTemp << endl;
			
			aff.push_back(getPerspectiveTransform(tmpMire, ptTemp));

		 	//cout << "result : " << aff[i] << endl;
			//aff.push_back(getPerspectiveTransform(ptTemp, tmp));
			
			/*
			 * Récupération des points parfaits et retour aux coordonnées de la mire
			 *//*
			//tmp[1].x += 32;
			//tmp[2].y += 24;
			//tmp[3].x += 32;
			//tmp[3].y += 24;
			Mat p0 = Mat ( pointsWorld [ 0 ] );
			Mat p1 = Mat ( pointsWorld [ 6 ] );
			Mat p2 = Mat ( pointsWorld [ 21 ] );
			Mat p3 = Mat ( pointsWorld [ 27 ] );
			pointImage.clear ();
			pointImage.push_back ( p0.clone() );
			pointImage.push_back ( p0.clone () );
			pointImage.push_back ( p0.clone () );
			pointImage.push_back ( p0.clone () );
			pointImage[1].at<double>(0,0) += 32;
			pointImage[2].at<double>(1,0) += 24;
			pointImage[3].at<double>(0,0) += 32;
			pointImage[3].at<double>(1,0) += 24;
			

			vector < Point3f > ptsWorldParfaite;
			vector < Point3f > ptsCamParfaite;
			vector < Point2f > ptsImgParfaite;
			vector < Point2f > ptsMireParfaite;
			for(j = 0; j < 4; j ++){
				cout << "--------------------------------" << endl;
				//cout << "start: " << tmp[j] << endl;
				//cout << "Input: " << pointImage[j].t() << endl;
				/*pointImage[j]*//* cv::Mat pC = actuelle.rvecs * pointImage[j] + actuelle.tvecs;
				//cout << "cams: " << pC.t() << endl;
				/*pointImage[j]*//* cv::Mat pP = pC / pC.at<double>(2,0);
				//cout << "proj: " << pP.t() << endl;
				/*pointImage[j]*//* cv::Mat pI = actuelle.cameraMatrix * pP;
				//cout << "image: " << pI.t() << endl;
				//cout << "mire: " << ptsMire[j] << endl;
				Mat pM = aff[i].inv(DECOMP_LU) * pI;
				//cout << "mire parf: " << pM.t() << endl;
				ptsWorldParfaite.push_back(Point3f(pointImage[j].at<double>(0,0), pointImage[j].at<double>(1,0), pointImage[j].at<double>(2,0)));
				ptsCamParfaite.push_back(Point3f(pC.at<double>(0,0), pC.at<double>(1,0), pC.at<double>(2,0)));
				ptsImgParfaite.push_back(Point2f(pI.at<double>(0,0), pI.at<double>(1,0)));
				ptsMireParfaite.push_back(Point2f(pM.at<double>(0,0), pM.at<double>(1,0)));
				pointImage[j]=pI;
			}

			//warpPerspective(ptsCamParfaite, ptsMireParfaite, aff[i], Size ( 4, 2 ) );
			cout << endl << "monde parfaite " << ptsWorldParfaite << endl;
			/*cout << endl << "monde extraite " << p0 << endl << p1 << endl << p2 << endl << p3 << endl;
			cout << endl << "image parfaite " << ptsImgParfaite << endl;
			cout << endl << "image extraite " << pointCam << endl;
			cout << endl << "mire parfaite " << ptsMireParfaite << endl;
			cout << endl << "mire extraite " << tmpMire << endl;*//*
			hmgy.push_back(getPerspectiveTransform(ptsMireParfaite, tmpMire));


			/* 
			 * Déformation de l'image a l'aide et enregistremenet
			 *//*
			
			warpPerspective(pict, deform[i], hmgy[i], pict.size(), WARP_INVERSE_MAP);
			//warpPerspective(pict, deform[i], aff[i], pict.size(), WARP_INVERSE_MAP);
			
			
			
			imshow ( "test", pict );
			imshow ( "warp", deform[i]);
			
			string str, outputFile;
			outputFile = "img";
			stringstream ss;
			ss << i;
			str = ss.str();
			outputFile += str;
			outputFile += ".png";
			vector<int> compression_params;
		    compression_params.push_back(CV_IMWRITE_PNG_COMPRESSION);
			compression_params.push_back(9);
			imwrite(outputFile, deform[i], compression_params);

*/
			//Boucle pour permettre l'affichage des comparaisons
			


	/*
	 * Déclaration des variables
	 */
	int i, j, nbCam, mode;
	double k;
	string fenetre;
	Size boardSize;
	char key;
	Mat cam, imgMire, positionCam;
	vector <Mat> ptsWorld, ptsWorldParfait, hmgy, affine, ptsCam;
	vector <Point3f> ptsCamParfait;
	vector <Point2f> ptsMire, ptsImg, ptsMireParfait, ptsImgParfait;
	bool found;
	pthread_t pthread_s;
	/*
	 * Initialisation
	 */
	const char ESC_KEY = 27;
	boardSize.height = 4;
	boardSize.width = 7;
	nbCam = argc - 1;
	imgMire = imread("mire.png", 1);

	/*
	 * Récupération des points de la mire
	 */
	if(mire(imgMire, boardSize, ptsMire))
		printf("\nPoints de l'image de la mire trouvés\n");
	else {
		printf("\nErreur : impossibilité de trouver les points de la mire\n");
		return EXIT_FAILURE;
	}

	for(i = 0; i < nbCam; i++){
		
		/*
		 * Remise à zéro des valeurs de traitement
		 */
		mode = 0;
		found = false;
		ptsWorld.clear();
		ptsWorldParfait.clear();
		ptsCam.clear();
		ptsCamParfait.clear();
		ptsImg.clear();
		ptsImgParfait.clear();
		destroyAllWindows();

		switch(i){
				case 0 :
					fenetre = "Image Caméra 1";
					actuelle = c1;
					namedWindow(fenetre);
					break;
				
				case 1 :
					fenetre = "Image Caméra 2";
					actuelle = c2;	
					namedWindow(fenetre);
					break;
				case 2 :
					if(argc >= 4){
						Camera * c = &c3;
						/*if (pthread_create(&thread_s, NULL, calibrStream, (void *)c)) {
							
							printf("pthread_create échoué.\n");
						}*/
					}
					pthread_cancel(thread_s);
					pthread_join(thread_s, NULL);
					break;
			}	
			
			while(key != 'q' && key != ESC_KEY && i < 2){
				
				found = false;
				cam = actuelle.image();

				if(cam.empty()){
					
					printf("Image vide pour la caméra %d.\n", i+1);
					break;
				}

				if(mode == 1)
					found = mire(cam, boardSize, ptsImg);
				drawChessboardCorners(cam, boardSize, Mat(ptsImg), found);
				
				imshow(fenetre, cam);

				key = (char)waitKey(50);

				/*
				 * Prise de décision en fonction de la touche pressée
				 */
				switch(key){
					 case 'c' :
						printf("%s", mode == 0? "Capture en cours\n": "");
						mode = 1;
						break;

					case 'u' :
						printf("%s", mode == 1 ? "Capture arretée\n": "");
						mode = 0;
						break;
				}
				/*
				 * Si la caméra a vu le damier, le traitement commence
				 */
				if(found)
					break;
			}

			if(found){
				
				printf("\nDamier trouvé par la caméra\n");

				/*
				 * Calcul de la position C de la caméra à l'aide des vecteurs de rotation 
				 * et de translation
				 */
				positionCam = -1 *(actuelle.rvecs.t() * actuelle.tvecs);
				cout << "affichage de la matrice de location de la caméra " << endl << positionCam << endl;
				
				printf("Transformation des %f points 2D image en points 3D caméra puis en points 3D du monde réel\n", ptsImg.size());
				
				for(j = 0; j < ptsImg.size(); j ++){
					
					ptsCam.push_back(Mat(3, 1, CV_64F));
					ptsWorld.push_back(Mat(3, 1, CV_64F));

					ptsCam[j].at<double>(0,0) = (ptsImg[j].x - actuelle.cameraMatrix.at<double>(0 ,2))/actuelle.cameraMatrix.at<double>(0,0);
					ptsCam[j].at<double>(0,0) = (ptsImg[j].x - actuelle.cameraMatrix.at<double>(1 ,2))/actuelle.cameraMatrix.at<double>(1,1);
					ptsCam[j].at<double>(2,0) = 1;

					cout << "Affichage temporaire des points caméra : Le point concerné " << j << " " << endl << ptsCam[j] << endl;
					/*
					 * Normalisation
					 */
					float norm = (sqrt(pow(ptsCam[j].at<double>(0,0), 2.0) + pow(ptsCam[j].at<double>(1,0), 2.0) + pow(ptsCam[j].at<double>(2,0), 2.0)));
					ptsCam[j] = (1.0/norm) * ptsCam[j];

					/*
					 * Multiplication par le vecteur de rotation
					 * obtention de la droite D entre le point du monde réel et la caméra
					 */
					ptsWorld[j] = actuelle.rvecs.t() * ptsCam[j];

					/* 
					 * Calcul des coordonnées du point P, point du monde réel dans le plan du mur
					 *  xP = xC + k*xD
					 *  yP = yC + k*yD
					 *  zP = zC + k*zD = 0 (car plan du mur)
					 *
					 *  donc k = - zC/zD
					 */
					k = -1.0 * positionCam.at<double>(2, 0)/ptsWorld[j].at<double>(2, 0);
					ptsWorld[j].at<double>(0,0) = positionCam.at<double>(0,0) + k * ptsWorld[j].at<double>(0,0);
					ptsWorld[j].at<double>(1,0) = positionCam.at<double>(1,0) + k * ptsWorld[j].at<double>(1,0);
					ptsWorld[j].at<double>(2,0) = positionCam.at<double>(2,0) + k * ptsWorld[j].at<double>(2,0);

					cout << "Affichage temporaire des calculs : Le point concerné " << j << " " << endl << ptsWorld[j] << endl << " et le coefficient " << k << endl;
					if(ptsWorld[j].at<double>(2,0) != 0){
						printf("Erreur de calcul : zP n'est pas égal à zéro, mais à : %f. k = %f, et zC = %f\n", ptsWorld[j].at<double>(2, 0), k, positionCam.at<double>(2,0));
						return EXIT_FAILURE;
					}
				}

				printf("Création des coordonées parfaites et transformation selon la déformation du projecteur\n");
				/*
				 * Récupération des quatres coins(0, 6, 21 27) de la mire, dans l'image de la mire, celle de la caméra et dans les points du monde réel
				 */
				vector <Point3f> coinWorldP;
				vector <Point2f> coinMire, coinImg;

				for(j = 0; j < 28; j +=6){
					
					coinImg.push_back(Point2f(ptsImg[j]));
					coinMire.push_back(Point2f(ptsMire[j]));
					ptsWorldParfait.push_back(ptsWorld[0].clone());

					if(j == 6)
						j = 15;
				}

				/*
				 * Récupération de la transformation affine entre la mire projetée et l'image filmée par la caméra
				 */
				affine.push_back(getPerspectiveTransform(coinMire, coinImg));

				/*
				 * Création des points du monde parfait
				 */
				ptsWorldParfait[1].at<double>(0,0) += 32;
				ptsWorldParfait[2].at<double>(1,0) += 24;
				ptsWorldParfait[3].at<double>(0,0) += 32;
				ptsWorldParfait[3].at<double>(1,0) += 24;

				for(j = 0; j < 4; j++){

					/*
					 * Passage des points parfaits du monde aux points parfaits de la caméra
					 */
					Mat ptCamP = actuelle.rvecs * ptsWorld[j] + actuelle.tvecs;
					ptCamP /= ptCamP.at<double>(2,0);
					ptsCamParfait.push_back(Point3f(ptCamP.at<double>(0,0), ptCamP.at<double>(1,0), ptCamP.at<double>(2,0)));

					/*
					 * Passage des points parfaits de la caméra aux points parfaits de l'image
					 */
					Mat ptImgP = actuelle.cameraMatrix * ptCamP;
					ptsImgParfait.push_back(Point2f(ptImgP.at<double>(0,0), ptImgP.at<double>(1,0)));
					/*
					 * Création des points parfaits de la mire
					 */
					Mat ptMireP = affine[i].inv(DECOMP_LU) * ptImgP;
					ptsMireParfait.push_back(Point2f(ptMireP.at<double>(0, 0), ptMireP.at<double>(1,0)));
				}

				/*
				 * Récupération de la matrice de transformation entre les points parfaits et les points de la mire
				 */
				hmgy.push_back(getPerspectiveTransform(ptsMireParfait, coinMire));

				/*
				 * Application de la matrice
				 */
				printf("Création de l'image transformée\n");
				Mat deformee;
				warpPerspective(imgMire, deformee, hmgy[i], imgMire.size(), WARP_INVERSE_MAP);

				/*
				 * Affichage et sauvegarde de l'image
				 */
				imshow ( "test", imgMire );
				imshow ( "warp", deformee);
				
				string str, nom;
				nom = "img";
				stringstream ss;
				ss << i;
				str = ss.str();
				nom += str;
				nom += ".png";
				vector<int> compression_params;
				compression_params.push_back(CV_IMWRITE_PNG_COMPRESSION);
				compression_params.push_back(9);
				imwrite(nom, deformee, compression_params);



				while(key != 'g')
					key = (char) waitKey(10);
		}
		if(key == 'q' || key == ESC_KEY){
			printf("\nChangement de caméra à la demande de l'utilisateur\n");
		}else{
			printf("\nChangement de caméra\n");
		}


	}
	
	destroyAllWindows();

	if(key == 'q' || key == ESC_KEY){
		printf("Programme terminé à la demande de l'utilisateur\n");
	}
	else{
	
		printf("Programme terminé normalement\n");
	}

	return EXIT_SUCCESS;
}
