#include <iostream>
#include <stdlib.h>
#include <sstream>
#include <time.h>
#include <stdio.h>
#include <X11/Xlib.h>
#include <pthread.h>

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/nonfree/nonfree.hpp"
#include "opencv2/nonfree/features2d.hpp"
#include "analyseProjecteur.hpp"

using namespace cv;
using namespace std;

/*
 * Fonctions générales
 */
void usage(){
	
	printf("Analyse de projecteur : \n Usage : analyse Fichier1 [Fichier2] [Fichier3]\n Les fichiers doivent être au format .xml ou .yml\n\n");
}

/*
 * Fonction pour la verification de la présence de la mire
 */
bool mire(Mat& img, Size boardSize, vector<Point2f>& pointBuf){
	
	bool found = false;
	found = findChessboardCorners( img, boardSize, pointBuf );//, CV_CALIB_CB_ADAPTIVE_THRESH | CV_CALIB_CB_FAST_CHECK | CV_CALIB_CB_NORMALIZE_IMAGE);
	if ( found){
		Mat viewGray;
		cvtColor(img, viewGray, COLOR_BGR2GRAY);
		cornerSubPix( viewGray, pointBuf, Size(11,11), Size(-1,-1), TermCriteria( CV_TERMCRIT_EPS+CV_TERMCRIT_ITER, 30, 0.1 ));
	}
	return found;
}

Mat surfMethode(Mat img, Mat pict){
	
	//-- Etape 1: Détecter les points d'interets en utilisant SURF
	
	int minHessian = 400;
	//SurfFeatureDetector detector(minHessian);

	OrbFeatureDetector detector;
	vector<KeyPoint> keypoints_object, keypoints_scene;

	detector.detect( pict, keypoints_object );
	detector.detect( img, keypoints_scene );

	//-- Etape 2: Calculer les descripteurs
	SurfDescriptorExtractor extractor;

	Mat descriptors_object, descriptors_scene;

	extractor.compute( pict, keypoints_object, descriptors_object );
	extractor.compute( img, keypoints_scene, descriptors_scene );

	//-- Etape 3: Faire correspondre les vecteurs de description en utilisant FLANN
	FlannBasedMatcher matcher;
	vector< DMatch > matches;
	matcher.match( descriptors_object, descriptors_scene, matches );

	double max_dist = 0; double min_dist = 100;

	//-- Calcul rapid des distances max et min entre les points d'interets
	for( int i = 0; i < descriptors_object.rows; i++ ){
		double dist = matches[i].distance;
		
		if( dist < min_dist ) 
			
			min_dist = dist;
		if( dist > max_dist ) 
			
			max_dist = dist;
	}

	printf("-- Max dist : %f \n", max_dist );
	printf("-- Min dist : %f \n", min_dist );

	//-- Dessiner seulement les bonnes correspondances
	vector< DMatch > good_matches;

	for( int i = 0; i < descriptors_object.rows; i++ ){
		
		if( matches[i].distance < 1.5*min_dist ){
			
			good_matches.push_back( matches[i]);
		}
	}
	
	Mat img_matches;
	drawMatches( pict, keypoints_object, img, keypoints_scene, good_matches, img_matches, Scalar::all(-1), Scalar::all(-1),	vector<char>(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS );

	//-- Localiser les objets
	vector<Point2f> obj;
	vector<Point2f> scene;

	for( int i = 0; i < good_matches.size(); i++ ){

		//-- Récuperer les points d'interets des bonnes corespondances
		obj.push_back( keypoints_object[ good_matches[i].queryIdx ].pt );
		scene.push_back( keypoints_scene[ good_matches[i].trainIdx ].pt );
	}

	printf("Taille de l'objet à analyser : %d, de la scene a analyser : %d\n", (int)obj.size(), (int)scene.size());
	Mat H = findHomography( obj, scene, CV_RANSAC );
	cout << "La matrice fondamentale est : " << endl << H << endl;


	//-- Récuperer les coins de l'image 1 (à detecter)
	vector<Point2f> obj_corners(4);
	obj_corners[0] = cvPoint(0,0); obj_corners[1] = cvPoint( pict.cols, 0 );
	obj_corners[2] = cvPoint(pict.cols, pict.rows ); obj_corners[3] = cvPoint( 0, pict.rows );
	vector<Point2f> scene_corners(4);

	perspectiveTransform( obj_corners, scene_corners, H);

	//-- Dessiner les lines entres les coins
	line( img_matches, scene_corners[0] + Point2f(pict.cols, 0), scene_corners[1] + Point2f(pict.cols, 0), Scalar(0, 255, 0), 4 );
	line( img_matches, scene_corners[1] + Point2f(pict.cols, 0), scene_corners[2] + Point2f(pict.cols, 0), Scalar( 0, 255, 0), 4 );
	line( img_matches, scene_corners[2] + Point2f(pict.cols, 0), scene_corners[3] + Point2f(pict.cols, 0), Scalar( 0, 255, 0), 4 );
	line( img_matches, scene_corners[3] + Point2f(pict.cols, 0), scene_corners[0] + Point2f(pict.cols, 0), Scalar( 0, 255, 0), 4 );

	return H;

}

Mat orbMethode(Mat img, Mat pict){
	
	//detect SIFT keypoints and extract descriptors in the train image
	vector<KeyPoint> train_kp;
	Mat train_desc;

	SiftFeatureDetector featureDetector;
	featureDetector.detect(pict, train_kp);
	SiftDescriptorExtractor featureExtractor;
	featureExtractor.compute(pict, train_kp, train_desc);

	// Brute Force based descriptor matcher object
	BFMatcher matcher;
	vector<Mat> train_desc_collection(1, train_desc);
	matcher.add(train_desc_collection);
	matcher.train();


	unsigned int frame_count = 0;

	double t0 = getTickCount();

	//detect SIFT keypoints and extract descriptors in the test image
	vector<KeyPoint> test_kp;
	Mat test_desc;
	featureDetector.detect(img, test_kp);
	featureExtractor.compute(img, test_kp, test_desc);

	// match train and test descriptors, getting 2 nearest neighbors for all test descriptors
	vector<vector<DMatch> > matches;
	matcher.knnMatch(test_desc, matches, 2);

	// filter for good matches according to Lowe's algorithm
	vector<DMatch> good_matches;
	for(int i = 0; i < matches.size(); i++) {
		
		if(matches[i][0].distance < 0.6 * matches[i][1].distance){
			
			good_matches.push_back(matches[i][0]);
		}
	}

	Mat img_show;
	drawMatches(img, test_kp, pict, train_kp, good_matches, img_show);
	imshow("Matches", img_show);

	cout << "Frame rate = " << getTickFrequency() / (getTickCount() - t0) << endl;
	
	vector<Point2f> obj;
	vector<Point2f> scene;

	for( int i = 0; i < good_matches.size(); i++ ){

		//-- Récuperer les points d'interets des bonnes corespondances
		obj.push_back( train_kp[ good_matches[i].queryIdx ].pt );
		scene.push_back( test_kp[ good_matches[i].trainIdx ].pt );
	}

	Mat H;

	printf("Taille de l'objet à analyser : %d, de la scene a analyser : %d. Taille de la boucle : %d. Nombre de correspondances : %d\n", (int)obj.size(), (int)scene.size(), (int)good_matches.size(), (int)matches.size());
	if((int)obj.size() > 0 && (int)scene.size() > 0){
		
		H = findHomography( obj, scene, CV_RANSAC);
		cout << "La matrice fondamentale est : " << endl << H << endl;
	}

	return H;
}
