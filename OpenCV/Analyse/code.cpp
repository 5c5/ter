/*
 * Déclaration des variables
 */
int i, j, nbcam, mode;
double k;
string fenetre;
Size boardSize;
const char ESC_KEY;
char key;
Mat cam, imgMire, positionCam;
vector <Mat> ptsWorld, ptsWorldParfait, hmgy, affine, ptsCam;
vector <Point3f> ptsCamParfait;
vector <Point2f> ptsMire, ptsImg, ptsMireParfait, ptsImgParfait;
bool found;
pthread_t pthread_s;
/*
 * Initialisation
 */
ESC_KEY = 27;
boardSize.height = 4;
boardSize.width = 7;
nbCam = argc - 1;
imgMire = imread("mire.png", 1);

/*
 * Récupération des points de la mire
 */
if(mire(imgMire, boardSize, ptsMire))
	printf("\nPoints de l'image de la mire trouvés\n");
else {
	printf("\nErreur : impossibilité de trouver les points de la mire\n");
	return EXIT_FAILURE;
}

for(i = 0; i < nbCam; i++){
	
	/*
	 * Remise à zéro des valeurs de traitement
	 */
	mode = 0;
	found = false;
	ptsWorld.clear();
	ptsWorldParfait.clear();
	ptsCam.clear();
	ptsCamParfait.clear();
	ptsImg.clear();
	ptsImgParfait.clear();
	destroyAllWindows();

	switch(i){
			case 0 :
				fenetre = "Image Caméra 1";
				actuelle = c1;
				namedWindow(fenetre);
				break;
			
			case 1 :
				fenetre = "Image Caméra 2";
				actuelle = c2;	
				namedWindow(fenetre);
				break;
			case 2 :
				if(argc >= 4){
					Camera * c = &c3;
					/*if (pthread_create(&thread_s, NULL, calibrStream, (void *)c)) {
						
						printf("pthread_create échoué.\n");
					}*/
				}
				pthread_cancel(thread_s);
				pthread_join(thread_s, NULL);
				break;
		}	
		
		while(key != 'q' && key != ESC_KEY && i < 2){
			
			found = false;
			cam = actuelle.image();

			if(cam.empty()){
				
				printf("Image vide pour la caméra %d.\n", i+1);
				break;
			}

			if(mode == 1)
				found = mire(cam, boardSize, ptsImg);
			drawChessboardCorners(cam, boardSize, Mat(ptsImg), found);
			
			imshow(fenetre, cam);

			key = (char)waitKey(50);

			/*
			 * Prise de décision en fonction de la touche pressée
			 */
			switch(key){
				 case 'c' :
				 	printf("%s", mode == 0? "Capture en cours\n": "");
					mode = 1;
					break;

				case 'u' :
					printf("%s", mode == 1 ? "Capture arretée\n": "");
					mode = 0;
					break;
			}
			/*
			 * Si la caméra a vu le damier, le traitement commence
			 */
			if(found)
				break;
		}

		if(found){
			
			printf("\nDamier trouvé par la caméra\n");

			/*
			 * Calcul de la position C de la caméra à l'aide des vecteurs de rotation 
			 * et de translation
			 */
			positionCam = -1 *(actuelle.rvecs.t() * actuelle.tvecs);
			
			printf("Transformation des points 2D image en points 3D caméra puis en points 3D du monde réel\n");
			for(j = 0; j < ptsImg.size(); j ++){
				
				ptsCam.push_back(Mat(3, 1, CV_64F));
				ptsWorld.push_back(Mat(3, 1, CV_64F));

				ptsCam[j].at<double>(0,0) = (ptsImg[j].x - actuelle.cameraMatrix.at<double>(0 ,2))/actuelle.cameraMatrix.at<double>(0,0);
				ptsCam[j].at<double>(0,0) = (ptsImg[j].x - actuelle.cameraMatrix.at<double>(1 ,2))/actuelle.cameraMatrix.at<double>(1,1);
				ptsCam[j].at<double>(2,0) = 1;

				/*
				 * Normalisation
				 */
				float norm = (sqrt(pow(ptsCam[j].at<double>(0,0), 2.0) + pow(ptsCam[j].at<double>(1,0), 2.0) + pow(ptsCam[j].at<double>(2,0), 2.0)));
				ptsCam[j] = (1.0/norm) * ptsCam[j];

				/*
				 * Multiplication par le vecteur de rotation
				 * obtention de la droite D entre le point du monde réel et la caméra
				 */
				ptsWorld[j] = actuelle.rvecs.t() * ptsCam[j];

				/* 
				 * Calcul des coordonnées du point P, point du monde réel dans le plan du mur
				 *  xP = xC + k*xD
				 *  yP = yC + k*yD
				 *  zP = zC + k*zD = 0 (car plan du mur)
				 *
				 *  donc k = - zC/zD
				 */
				k = -1.0 * positionCam.at<double>(2, 0)/ptsWorld[j].at<double>(2, 0);
				ptsWorld[j].at<double>(0,0) = positionCam.at<double>(0,0) + k * ptsWorld[j].at<double>(0,0);
				ptsWorld[j].at<double>(1,0) = positionCam.at<double>(1,0) + k * ptsWorld[j].at<double>(1,0);
				ptsWorld[j].at<double>(2,0) = positionCam.at<double>(2,0) + k * ptsWorld[j].at<double>(2,0);
				
				if(ptsWorld[j].at<double>(2,0) != 0){
					printf("Erreur de calcul : zP n'est pas égal à zéro, mais à : %f. k = %f, et zC = %f\n", ptsWorld[j].at<double>(2, 0), k, positionCam.at<double>(2,0));
					return EXIT_FAILURE;
				}
			}

			printf("Création des coordonées parfaites et transformation selon la déformation du projecteur\n");
			/*
			 * Récupération des quatres coins(0, 6, 21 27) de la mire, dans l'image de la mire, celle de la caméra et dans les points du monde réel
			 */
			vector <Point3f> coinWorldP;
			vector <Point2f> coinMire, coinImg;

			for(j = 0; j < 28; j +=6){
				
				coinImg.push_back(ptsImg[j].clone());
				coinMire.push_back(ptsMire[j].clone());
				ptsWorldParfait.push_back(ptsWorld[0].clone());

				if(j == 6)
					j = 15;
			}

			/*
			 * Récupération de la transformation affine entre la mire projetée et l'image filmée par la caméra
			 */
			aff.push_back(getPerspectiveTransform(coinMire, coinImg));

			/*
			 * Création des points du monde parfait
			 */
			ptsWorldP[1].at<double>(0,0) += 32;
			ptsWorldP[2].at<double>(1,0) += 24;
			ptsWorldP[3].at<double>(0,0) += 32;
			ptsWorldP[3].at<double>(1,0) += 24;

			for(j = 0; j < 4; j++){

				/*
				 * Passage des points parfaits du monde aux points parfaits de la caméra
				 */
				Mat ptCamP = actuelle.rvecs * ptsWorld[j] + actuelle.tvecs;
				ptCamP /= ptCamP.at<double>(2,0);
				ptsCamParfait.push_back(Point3f(ptCamP.at<double>(0,0), ptCamP.at<double>(1,0), ptCamP.at<double>(2,0)));

				/*
				 * Passage des points parfaits de la caméra aux points parfaits de l'image
				 */
				Mat ptImgP = actuelle.cameraMatrix
				ptsImgParfait.push_back(Point2f(ptImgP.at<double>(0,0), ptImgP.at<double>(1,0)));
				/*
				 * Création des points parfaits de la mire
				 */
				Mat ptMireP = aff[i].inv(DECOMP_LU) * ptImgP;
				ptsMireParfait.push_back(Point2f(ptMireP.at<double>(0, 0), ptMireP.at<double>(1,0)));
			}

			/*
			 * Récupération de la matrice de transformation entre les points parfaits et les points de la mire
			 */
			hgmy.push_back(getPerspectiveTransform(ptsMireParfait, coinMire));

			/*
			 * Application de la matrice
			 */
			printf("Création de l'image transformée\n");
			Mat deformee;
			warpPerspective(pict, deformee, hgmy[i], pict.size(), WARP_INVERSE_MAP);

			/*
			 * Affichage et sauvegarde de l'image
			 */
			imshow ( "test", pict );
			imshow ( "warp", deformee);
			
			string str, nom;
			nom = "img";
			stringstream ss;
			ss << i;
			str = ss.str();
			nom += str;
			nom += ".png";
			vector<int> compression_params;
		    compression_params.push_back(CV_IMWRITE_PNG_COMPRESSION);
			compression_params.push_back(9);
			imwrite(nom, deformee, compression_params);

