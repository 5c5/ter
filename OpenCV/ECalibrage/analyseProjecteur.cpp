#include <iostream>
#include <stdlib.h>
#include <sstream>
#include <time.h>
#include <stdio.h>
#include <X11/Xlib.h>
#include <pthread.h>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/highgui/highgui.hpp>

#include "analyseProjecteur.hpp"

#ifndef _CRT_SECURE_NO_WARNINGS
# define _CRT_SECURE_NO_WARNINGS
#endif

using namespace cv;
using namespace std;

/*
 * Signature de la fonction de stream
 */
void* calibrStream(void * arg);



/* 
 * Déclaration des variables globales, normalement atteignables pour chauqe processus
 * */

/*
 * Classe contenant les paramètres (instrinsèques) d'une caméra
 * */
/*class Camera {
public:
	
	Camera() : goodInput(false){}
*/	
	void Camera::read(const FileNode& node){

        node["Numero_camera" ] >> cameraID;
		//cout << "\nNumero de la camera : " << cameraID << endl;
        node["Camera_Matrix" ] >> cameraMatrix;
		//cout << "Matrice de projection " << endl << " " << cameraMatrix << endl << endl;

        node["Distortion_Coefficients" ] >> distcoefs;
		//cout << "Coefficient de distorsion " << endl << " " << distcoefs << endl << endl;
		printf("Activation de la camera %d\n", cameraID);
		activation();
		printf("Camera activée\n");
	}

	void Camera::write(FileStorage& fs){

		fs << "{" << "Numero_Camera" << cameraID
				  << "Camera_Matrix" << cameraMatrix
				  << "Distorsion_Coefficients" << distcoefs
			<< "}";
	}

	void Camera::activation(){

		goodInput = true;

		if(cameraID < 0 || cameraID > 2){
			
			goodInput = false;

			printf("Valeur incorrecte pour l'ID de la caméra : %d\n", cameraID);
		}
		else {
				if(inputCapture.open(cameraID))
					printf("Caméra N°%d ouverte\n", cameraID);
				else
					printf("Camera N°%d n'a pas été ouverte\n", cameraID);
		}

		if(cameraMatrix.rows !=3 || cameraMatrix.cols !=3){
			
			goodInput = false;
			printf("La matrice de paramètre instrinsèque n'a pas les bonnes dimensions : %dx%d\n", cameraMatrix.rows, cameraMatrix.cols);
		}
		outputFile = "params";
		stringstream ss;
		ss << cameraID;
		string str = ss.str();
		outputFile += str;
		outputFile += ".yml";
		rvecs.create(3, 3, CV_64F);
		tvecs.create(3, 3, CV_64F);
		

	}

	Mat Camera::image(){
		
		Mat result;
        if( inputCapture.isOpened() ){
			
			Mat view0;
            inputCapture >> view0;
            view0.copyTo(result);
		}
		return result;
	}

	void Camera::sauvegarde(){
		
		FileStorage fs(outputFile, FileStorage::WRITE );

		fs << "Numero_Camera" << cameraID;
		fs << "Camera_Matrix" << cameraMatrix;
    	fs << "Distortion_Coefficients" << distcoefs;
		
		if( !rvecs.empty() && !tvecs.empty() ){

			CV_Assert(rvecs.type() == tvecs.type());
			Mat bigmat(1, 6, rvecs.type());
			//for( int i = 0; i < (int)rvecs.size(); i++ ){

				//Mat r = bigmat(Range(0, 0+1), Range(0,3));
				//Mat t = bigmat(Range(0, 0+1), Range(3,6));

				//CV_Assert(rvecs.rows == 3 && rvecs.cols == 1);
				//CV_Assert(tvecs.rows == 3 && tvecs.cols == 1);
				//*.t() is MatExpr (not Mat) so we can use assignment operator
				//r = rvecs.t();
				//t = tvecs.t();
			//}
			cvWriteComment( *fs, "a set of 6-tuples (rotation vector + translation vector) for each view", 0 );
			//fs << "Extrinsic_Parameters" << bigmat;
			fs << "Rotation_Vector" << rvecs;
			fs << "Translation_Vector" << tvecs;
		}

	}
	
	void Camera::calcBoardCornerPositions(Size boardSize, float squareSize, vector<Point3f>& corners){
	
		corners.clear();
		for( int i = 0; i < boardSize.height; ++i )
			for( int j = 0; j < boardSize.width; ++j )
				corners.push_back(Point3f(float( j*squareSize ), float( i*squareSize ), 0));
	}

	bool Camera::calibrationExtrinsec(vector<vector<Point2f> > imagePoints, Size boardSize){

		//cv::Mat r ( 3, 3, CV_32F ), t ( 3, 3, CV_32F );
		vector<vector<Point3f> > objectPoints(1);
		calcBoardCornerPositions(boardSize, 4, objectPoints[0]);
		objectPoints.resize(imagePoints.size(),objectPoints[0]);

		/*printf("Affichage de objectPoint et imagePoint :\n");
		cout << "Object point : \n " << endl <<objectPoints[0] << endl << endl;
		cout << "imagePoint : \n " << endl <<  imagePoints[0] << endl << endl;
		printf("Affichage de tvecs et rvecs\n");
		//cout << "tvecs : \n " << endl <<  tvecs[0] << endl << endl;
		//cout << "rvecs : \n " << endl <<  rvecs[0] << endl << endl;	

		printf("\nTentative de résolution du problème\n");
		*/
		bool ret = solvePnP(objectPoints[0], imagePoints[0], cameraMatrix, distcoefs, rvecs, tvecs);

		if(ret){
			Rodrigues(rvecs, rvecs);
			//Rodrigues(tvecs, tvecs);
		}
		return ret;

	}
/*

public:
	int cameraID;
	Mat cameraMatrix;
	Mat distcoefs;
	vector<Mat> rvecs;
	vector<Mat> tvecs;
	VideoCapture inputCapture;
	bool goodInput;
	string outputFile;
};*/

void usage(){
	
	printf("Analyse de projecteur : \n Usage : analyse Fichier1 [Fichier2] [Fichier3]\n Les fichiers doivent être au format .xml ou .yml\n");
}

/*
 * Fonction pour la verification de la présence de la mire
 */
bool mire(Mat& img, vector<Point2f>& pointBuf, Size boardSize){
		
		bool found = false;
		found = findChessboardCorners( img, boardSize, pointBuf, CV_CALIB_CB_ADAPTIVE_THRESH | CV_CALIB_CB_FAST_CHECK | CV_CALIB_CB_NORMALIZE_IMAGE);
        if ( found){
			Mat viewGray;
			cvtColor(img, viewGray, COLOR_BGR2GRAY);
			cornerSubPix( viewGray, pointBuf, Size(11,11), Size(-1,-1), TermCriteria( CV_TERMCRIT_EPS+CV_TERMCRIT_ITER, 30, 0.1 ));
		}
		return found;
}


/*
 * Fonction principale prenant un, deux ou trois argumments, c'est à dire les fichiers contenant les paramètres instrinsèques de la ou des caméras.
 * */
int main(int argc, char* argv[]){

	XInitThreads();
	printf("Programme de lecture des images projetées par les vidéo-projecteurs\n");
	printf("Les fichiers contenant les paramètres intrinsèques de la caméra 1 (et de la 2 si existante) sont passés en paramètres du programme\n");
	
	if(argc < 2){
		
		usage();
		return EXIT_FAILURE;
	}

	Mat img;
	pthread_t thread_s;
	Camera c1, c2, c3, actuelle;
	const string inputFile1 = argc > 1? argv[1] : "";
	const string inputFile2 = argc > 2? argv[2] : "";
	const string inputFile3 = argc > 3? argv[3] : "";
	/*
	 * Ouverture du premier fichier de paramètre
	 * */
	FileStorage fs(inputFile1, FileStorage::READ);
	if (!fs.isOpened()){
        cout << "Echec de l'ouverture du fichier de paramètre : \"" << inputFile1 << "\"" << endl;
        return EXIT_FAILURE;
    }
	else {
		c1.read(fs["Settings"]);
		fs.release();
		if (!c1.goodInput){
			cout << "Erreur lors de la lecture de :\"" << inputFile1 <<"\". Arret du programme." << endl;
			return EXIT_FAILURE;
			}
	}

	/*
	 * Ouverture du second fichier de paramètre
	 * */
	if(argc > 2)
		fs.open(inputFile2, FileStorage::READ); // Lecture des paramètres de la seconde caméra
	if (!fs.isOpened() && argc > 2){
        
		cout << "Echec de l'ouverture du fichier de paramètre: \"" << inputFile2 << "\"" << endl;
        return EXIT_FAILURE;
    }
    c2.read(fs["Settings"]);
    fs.release();
	if (!c2.goodInput){
        
		cout << "Erreur lors de la lecture de : \"" << inputFile2 <<"\". Arrêt du programme." << endl;
        return EXIT_FAILURE;
    }

	/*
	 * Ouverture du troisième fichier de paramètre
	 * */
	if(argc > 3){
		fs.open(inputFile3, FileStorage::READ); // Lecture des paramètres de la troisième caméra
		if (!fs.isOpened()){
			
			cout << "Echec de l'ouverture du fichier de paramètre: \"" << inputFile3 << "\"" << endl;
			return EXIT_FAILURE;
		}
		else{
			c3.read(fs["Settings"]);
			fs.release();
			
			if (!c3.goodInput){
			
				cout << "Erreur lors de la lecture de : \"" << inputFile3 <<"\". Arrêt du programme." << endl;
				return EXIT_FAILURE;
			}
		}
	}


	/*
	 * Déclaration des variables d'affichage et de traitement
	 * */
	const char ESC_KEY = 27;
	string fenetre;
	Size boardSize;
	boardSize.height = 4,
	boardSize.width = 5;
	clock_t prevTimestamp = 0;		
	char key = 'a';
	int nbCam = argc -1;

	for(int i= 0; i < nbCam ; i ++){
	
		int mode = 0;
		key = 'a';
		vector<vector<Point2f> > imagePoints;
		destroyAllWindows();

		printf("\nCalibration de la camera %d lancee\n", i+1);

		switch(i){
			case 0 :
				fenetre = "Image Caméra 1";
				actuelle = c1;
				namedWindow(fenetre);
				break;
			
			case 1 :
				fenetre = "Image Caméra 2";
				actuelle = c2;	
				namedWindow(fenetre);
				break;
			case 2 :
				if(argc >= 4){
					Camera * c = &c3;
					if (pthread_create(&thread_s, NULL, calibrStream, (void *)c)) {
						
						printf("pthread_create échoué.\n");
					}
				}
				pthread_cancel(thread_s);
				pthread_join(thread_s, NULL);
				break;
		}	
		
		while(key != 'q' && key != ESC_KEY && i < 2){
			
			//Variables pour la reconnaissance de la mire.
			vector<Point2f> pointBuf;
			bool found = false;

			img = actuelle.image();

			if(img.empty()){
				
				printf("Image vide pour la caméra %d.\n", i+1);
				break;
			}
			if(mode == 1){
				found = mire(img, pointBuf, boardSize);

				bitwise_not(img, img);
			}
				
			drawChessboardCorners(img, boardSize, Mat(pointBuf), found);

			imshow(fenetre, img);

			key = (char)waitKey(50);

			switch(key){
				 case 'c' :
					mode = 1;
					break;

				case 'u' :
					mode = 0;
					break;
			}
			if(found){
				
				printf("Mire reconnue pour la caméra %d.\n", i+1);
				imagePoints.push_back(pointBuf);
				break;
			}

		}

		/*
		 * Calibrage extrinsèque des caméras si possible
		 */
		if(!imagePoints.empty() && i < 2){

			actuelle.calibrationExtrinsec(imagePoints, boardSize);
			printf("Calibration faite pour caméra %d\n", i+1);
			actuelle.sauvegarde();
			printf("Sauvegarde faite pour caméra %d\n", i+1);
		}
	}
	
	destroyAllWindows();

	if(key == 'q' || key == ESC_KEY){
		printf("Programme terminé à la demande de l'utilisateur\n");
	}
	else{
	
		printf("Programme terminé normalement\n");
	}

	return EXIT_SUCCESS;
}
