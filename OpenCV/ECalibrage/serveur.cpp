#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <iostream>
#include <string>
#include <pthread.h>
#include <sstream>
#include <X11/Xlib.h>
#include <unistd.h>

#include "analyseProjecteur.hpp"

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/highgui/highgui.hpp>

using namespace cv;
using namespace std; 


int     is_data_ready = 0;
int     listenSock , connectSock;
int 	listenPort = 4800;

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

Mat stream;

void* streaming(void* arg);
void  quit(string msg, int retval);

/*
 * Cette fonction récupère les paramètres extrinsèques d'une caméra via un streaming
 */
void* calibrStream(void * arg){
	
	/*
	 * Déclaration des variables pour l'extraction des paramètres intrinsèques
	 */

	pthread_t thread_s;
	const char ESC_KEY = 27;
	int mode = 0;
	Mat img;
	Size boardSize;
	boardSize.height = 4;
	boardSize.width = 7;
	clock_t prevTimestamp = 0;
	Camera* c;
	Camera ca;
	c =(Camera*)arg;
	ca = *c;
	char key;
	stream = Mat::zeros( 480, 640, CV_8UC1);

	vector<vector<Point2f> > imagePoints;

	/* 
	 * Ce processus ne peut être tué en utilisant pthread_cancel
	 */
	pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL);
	pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);



	/*
	 * Création du processus de streaming
	 */
	if (pthread_create(&thread_s, NULL, streaming, NULL)) {
		quit("pthread_create failed.", 1);
	}


	/* Boucle d'affichage des images reçue via le stream
	 */
	while(key != 'q' && key != ESC_KEY) {

        vector<Point2f> pointBuf;
        bool found  = false;

		pthread_mutex_lock(&mutex);
		
		if (is_data_ready) {
			
			img = stream;

			if(img.empty()){
				printf("Image vide pour la caméra 3. Arret du processus\n");
				break;
			}
			if(mode == 1){
				found = findChessboardCorners( img, boardSize, pointBuf, CV_CALIB_CB_ADAPTIVE_THRESH | CV_CALIB_CB_FAST_CHECK | CV_CALIB_CB_NORMALIZE_IMAGE);

				if ( found){
					/*Mat viewGray;
					cvtColor(img, viewGray, COLOR_BGR2GRAY);*/
					cornerSubPix( img, pointBuf, Size(11,11), Size(-1,-1), TermCriteria( CV_TERMCRIT_EPS+CV_TERMCRIT_ITER, 30, 0.1 ));
					
					if(clock() - prevTimestamp > 100*1e-3*CLOCKS_PER_SEC){
						prevTimestamp = clock();
					}
				}
				bitwise_not(img, img);
			}

			drawChessboardCorners( img, boardSize, Mat(pointBuf), found );

			imshow("Image Caméra 3", img);
			is_data_ready = 0;
		}
		
		pthread_mutex_unlock(&mutex);
		key = (char) waitKey(10);
		switch(key){

			case 'c' :
				mode =1;
				break;

			case 'u':
				mode = 0;
				break;
		}
		if(found){
			imagePoints.push_back(pointBuf);
			break;
		}
	}

	if(!imagePoints.empty()){
		ca.calibrationExtrinsec(imagePoints, boardSize);
		ca.sauvegarde();
		printf("Caméra 3 calibrée et paramètres sauvegardés\n");
	}

	pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);

	printf("Thread de gestion de la caméra terminé\n");
	int res = pthread_cancel(thread_s);
	printf("Le thread de streaming %s correctement terminé\n", res ? "s'est": "ne s'est pas");

}

/////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Cette fonction permet de lire le flux video passé en paramètre
 */
void* streaming(void* arg){

	struct  sockaddr_in serverAddr, clientAddr;
	socklen_t clientAddrLen = sizeof(clientAddr);

	/* make this thread cancellable using pthread_cancel() */
	pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
	pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);

	if ((listenSock = socket(PF_INET, SOCK_STREAM, 0)) < 0) {
		quit("socket() échoué\n.", 1);
	}
		
	serverAddr.sin_family = PF_INET;
	serverAddr.sin_addr.s_addr = htonl(INADDR_ANY);
	serverAddr.sin_port = htons(listenPort);

	if (bind(listenSock, (sockaddr*)&serverAddr, sizeof(serverAddr)) == -1) {
			quit("bind() échoué\n", 1);
	}
			
	if (listen(listenSock, 5) == -1) {
			quit("listen() échoué\n.", 1);
	}
	
	int  imgSize = stream.total()*stream.elemSize();
	char sockData[imgSize];
	int  bytes=0;
			
	/* boucle de réception des images */
	while(1){
		
		cout << "-->En attente d'une connection sur le port " << listenPort << " ...\n\n";
	
		/* accept a request from a client */
		if ((connectSock = accept(listenSock, (sockaddr*)&clientAddr, &clientAddrLen)) == -1) {
			quit("accept() échoué\n", 1);
		}else{
			cout << "-->Reception d'une image de " << inet_ntoa(clientAddr.sin_addr) << ":" << ntohs(clientAddr.sin_port) << "..." << endl;
		
		
			memset(sockData, 0x0, sizeof(sockData));
			
			while(1){

				for (int i = 0; i < imgSize; i += bytes) {
					if ((bytes = recv(connectSock, sockData +i, imgSize  - i, 0)) == -1) {
						
						quit("recv failed", 1);
					}
				}
				/* convert the received data to OpenCV's Mat format, thread safe */
				pthread_mutex_lock(&mutex);
				for (int i = 0;  i < stream.rows; i++) {
					for (int j = 0; j < stream.cols; j++) {
							(stream.row(i)).col(j) = (uchar)sockData[((stream.cols)*i)+j];
					}
				}
				is_data_ready = 1;
				memset(sockData, 0x0, sizeof(sockData));
				pthread_mutex_unlock(&mutex);
			}
		}

	}
}
/**
 * Cette fonction permet de quitter proprement le programme
 */
void quit(string msg, int retval){
        if (retval == 0) {
                cout << (msg == "NULL" ? "" : msg) << "\n" <<endl;
        } else {
                cerr << (msg == "NULL" ? "" : msg) << "\n" <<endl;
        }
         
        if (listenSock){
                close(listenSock);
        }

        if (connectSock){
                close(connectSock);
        }
                                
        if (!stream.empty()){
                (stream.release());
        }
                
        pthread_mutex_destroy(&mutex);
        exit(retval);
}

