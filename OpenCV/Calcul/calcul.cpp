#include <iostream>
#include <stdlib.h>
#include <sstream>
#include <time.h>
#include <stdio.h>
#include <X11/Xlib.h>
#include <pthread.h>
#include <math.h>

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/nonfree/nonfree.hpp"
#include "opencv2/nonfree/features2d.hpp"
#include "calcul.hpp"

#ifndef _CRT_SECURE_NO_WARNINGS
# define _CRT_SECURE_NO_WARNINGS
#endif

using namespace cv;
using namespace std;


/* 
 * Déclaration des variables globale * 
 */


/*
 * méthodes de la classe Caméra contenant les paramètres d'une caméra
 * */
/*
 * Lecture des paramètres depuis un fichier
 */
void Camera::read(const FileNode& node){

	node["Numero_Camera" ] >> cameraID;
	printf("Affichage du numéro de la caméra : %d\n", cameraID);
	node["Camera_Matrix" ] >> cameraMatrix;
	node["Distortion_Coefficients" ] >> distcoefs;
	node["Rotation_Vector"] >> rvecs;
	node["Translation_Vector"] >> tvecs;
	printf("Activation de la camera %d\n", cameraID+1);
	activation();
	printf("La camera %d %s activée\n\n", cameraID+1, goodInput ? "s'est correctement" : "ne s'est pas correctement");
}

/*
 * Écriture des paramètres (obsolète)
 */
void Camera::write(FileStorage& fs){

	fs << "{" << "Numero_Camera" << cameraID
			  << "Camera_Matrix" << cameraMatrix
			  << "Distorsion_Coefficients" << distcoefs
			  << "Rotation_Vector" << rvecs
			  << "Translation_Vector" << tvecs
		<< "}";
}

/*
 * Vérification des valeurs des paramètres et activation de la caméra
 */
void Camera::activation(){

	goodInput = true;

	if(cameraID < 0 || cameraID > 2){
		
		goodInput = false;

		printf("Valeur incorrecte pour l'ID de la caméra : %d\n", cameraID+1);
	}
	else {
			if(inputCapture.open(cameraID)){
				printf("Caméra N°%d ouverte\n", cameraID+1);
				//inputCapture.set(CV_CAP_PROP_FRAME_WIDTH, 640);
				//inputCapture.set(CV_CAP_PROP_FRAME_HEIGHT, 480);
			}
			else{
				printf("Camera N°%d n'a pas été ouverte\n", cameraID+1);
				if(cameraID < 2)
					goodInput = false;
			}
	}

	if(cameraMatrix.rows !=3 || cameraMatrix.cols !=3){
		
		goodInput = false;
		printf("La matrice de paramètre instrinsèque n'a pas les bonnes dimensions : %dx%d\n", cameraMatrix.rows, cameraMatrix.cols);
	}
	
	if(rvecs.rows != 3 || rvecs.cols != 3){

		goodInput = false;
		printf("Le vecteur de rotation n'a pas les bonnes dimensions : %dx%d\n", rvecs.rows, rvecs.cols);
	}
	
	if(tvecs.rows != 3 || tvecs.cols != 1){

		goodInput = false;
		printf("Le vecteur de translation n'a pas les bonnes dimensions : %dx%d\n", tvecs.rows, tvecs.cols);
	}

}

/*
 * Récupération de la nouvelle image
 */
Mat Camera::image(){
		
	Mat result;
	if( inputCapture.isOpened() ){
		
		Mat view0;
		inputCapture >> view0;
		view0.copyTo(result);
	}
	return result;
}


/*
 * Signatures des fonctions
 */
void usage();
bool mire(Mat& img, Size boardSize, vector<Point2f>& buf);

/*
 * Fonction principale prenant un, deux ou trois argumments, c'est à dire les fichiers contenant les paramètres instrinsèques de la ou des caméras.
 * */
int main(int argc, char* argv[]){

	printf("Programme de calcul de la zone de projection optimale\n");
	printf("Les fichiers contenant les paramètres intrinsèques de la caméra 1 (et de la 2 si existante) sont passés en paramètres du programme\n\n");
	
	if(argc < 2){
		
		usage();
		return EXIT_FAILURE;
	}

	/*
	 * Déclaration et initialisation des variables du programme
	 */
	pthread_t thread_s;
	Camera c1, c2, c3, actuelle;
	const string inputFile1 = argc > 1? argv[1] : "";
	const string inputFile2 = argc > 2? argv[2] : "";
	const string inputFile3 = argc > 3? argv[3] : "";

	/*
	 * Ouverture du premier fichier de paramètre
	 * */
	FileStorage fs(inputFile1, FileStorage::READ);
	if (!fs.isOpened()){
        cout << "Echec de l'ouverture du fichier de paramètre : \"" << inputFile1 << "\"" << endl;
        return EXIT_FAILURE;
    }
	else {
		cout << "Nom du fichier ouvert : " << inputFile1 << endl;
		c1.read(fs["Settings"]);
		fs.release();
		if (!c1.goodInput){
			cout << "Erreur lors de la lecture de :\"" << inputFile1 <<"\". Arret du programme." << endl;
			return EXIT_FAILURE;
			}
	}
	/*
	 * Ouverture du second fichier de paramètre
	 * */
	if(argc > 2)
		fs.open(inputFile2, FileStorage::READ); // Lecture des paramètres de la seconde caméra
	if (!fs.isOpened() && argc > 2){
        
		cout << "Echec de l'ouverture du fichier de paramètre: \"" << inputFile2 << "\"" << endl;
        return EXIT_FAILURE;
    }else {
		cout << "Nom du fichier ouvert : " << inputFile2 << endl;
    	c2.read(fs["Settings"]);
    	fs.release();
		if (!c2.goodInput){
        
			cout << "Erreur lors de la lecture de : \"" << inputFile2 <<"\". Arrêt du programme." << endl;
        	return EXIT_FAILURE;
		}
    }
	/*
	 * Ouverture du troisième fichier de paramètre
	 * */
	if(argc > 3){
		fs.open(inputFile3, FileStorage::READ); // Lecture des paramètres de la troisième caméra
		if (!fs.isOpened()){
			
			cout << "Echec de l'ouverture du fichier de paramètre: \"" << inputFile3 << "\"" << endl;
			return EXIT_FAILURE;
		}
		else{
			c3.read(fs["Settings"]);
			fs.release();
			
			if (!c3.goodInput){
			
				cout << "Erreur lors de la lecture de : \"" << inputFile3 <<"\". Arrêt du programme." << endl;
				return EXIT_FAILURE;
			}
		}
	}



	/*
	 * Déclaration des variables d'affichage et de traitement, qui ne sont nécessaires que si toutes les caméras sont correctement ouvertes
	 * */
	string fenetre;
	int i, j, k, nbCam, mode, vp;
	Size boardSize;		
	char key;
	Mat img;
	bool found;
	vector<Point2f> ptsCam, coinZone[2];

	/*
	 * Initialisation des variables
	 */

	const char ESC_KEY = 27;
	boardSize.height = 4,
	boardSize.width = 7;
	nbCam = argc -1;
	vp = 1;
	
	for(i= 0; i < nbCam ; i ++){
	
		mode = 0;
		key = 'a';
		destroyAllWindows();


		switch(i){
			case 0 :
				fenetre = "Image Caméra 1";
				actuelle = c1;
				namedWindow(fenetre);
				break;
			
			case 1 :
				fenetre = "Image Caméra 2";
				actuelle = c2;	
				namedWindow(fenetre);
				break;
			case 2 :
				if(argc >= 4){
					Camera * c = &c3;
					/*if (pthread_create(&thread_s, NULL, calibrStream, (void *)c)) {
						
						printf("pthread_create échoué.\n");
					}*/
				}
				pthread_cancel(thread_s);
				pthread_join(thread_s, NULL);
				break;
		}
		for(j = 0; j < 2; j++){

			mode = 0;
			printf("Vidéo projecteur %i analysé\n", j+1);
			destroyWindow("Rectangle");
			
			while(key != 'q' && key != ESC_KEY && i < 2){
				
				found = false;
				img = actuelle.image();

				if(img.empty()){
					
					printf("Image vide pour la caméra %d.\n", i+1);
					break;
				}

				if(mode == 1)
					found = mire(img, boardSize, ptsCam);
				//drawChessboardCorners(img, boardSize, Mat(ptsCam), found);
				
				imshow(fenetre, img);

				key = (char)waitKey(50);

				switch(key){
					 case 'c' :
						printf("%s", mode == 0? "Capture on\n": "");
						mode = 1;
						break;

					case 'u' :
						printf("%s", mode == 1 ? "Capture off\n": "");
						mode = 0;
						break;
				}
				if(found)
					break;
			}
			if(found){

				for(k = 0; k < 28; k+=6){
					coinZone[j].push_back(Point2f(ptsCam[k]));

					if(k == 6)
						k=15;
				}

				coinZone[j][0].x -= 100;
				coinZone[j][0].y -= 60;

				coinZone[j][1].x += 95;
				coinZone[j][1].y -= 60;

				coinZone[j][2].x -= 80;
				coinZone[j][2].y += 40;
				coinZone[j][3].x += 85;
				coinZone[j][3].y += 50;



				line(img, coinZone[j][0], coinZone[j][1], Scalar(100, 100, 0));
				line(img, coinZone[j][0], coinZone[j][2], Scalar(100, 0, 0));
				line(img, coinZone[j][1], coinZone[j][3], Scalar(0, 100, 0));
				line(img, coinZone[j][2], coinZone[j][3], Scalar(0, 0, 100));
				//rectangle(img, coinZone[j][0], coinZone[j][3], Scalar(0, 0, 0));
				imshow("Rectangle", img);

				while(key != 'n')
					key = (char) waitKey(10);
			
				if(key == 'q' || key == ESC_KEY){
					
					printf("\n%s\n", j==0? "Changement de vidéo-projecteur à la demande de l'utilisateur": "Changement de caméra à la demande de l'utilisateur");
				}else{
					
					printf("\n%s\n", j==0? "Changement de vidéo projecteur": "Changement de caméra");
				}
			}
		}

	}
	
	destroyAllWindows();

	if(key == 'q' || key == ESC_KEY){
		
		printf("Programme terminé à la demande de l'utilisateur\n");
	}
	else{
	
		printf("Programme terminé normalement\n");
	}

	return EXIT_SUCCESS;
}
