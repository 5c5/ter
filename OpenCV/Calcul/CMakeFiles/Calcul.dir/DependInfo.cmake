# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/Students/p1410822/TER/OpenCV/Calcul/calcul.cpp" "/Students/p1410822/TER/OpenCV/Calcul/CMakeFiles/Calcul.dir/calcul.cpp.o"
  "/Students/p1410822/TER/OpenCV/Calcul/fonctions.cpp" "/Students/p1410822/TER/OpenCV/Calcul/CMakeFiles/Calcul.dir/fonctions.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "/Locals/OpenCV-2.4.10/include/opencv"
  "/Locals/OpenCV-2.4.10/include"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
