#include <iostream>
#include <sstream>
#include <time.h>
#include <stdio.h>
#include <X11/Xlib.h>
#include <pthread.h>

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/highgui/highgui.hpp>

using namespace cv;
using namespace std;



#ifndef ANALYSE_H
#define ANALYSE_H


/*
 * Classe contenant les paramètres d'une caméra
 * */
class Camera {
public:
	
	Camera() : goodInput(false){}
	
	void read(const FileNode& node);
	void write(FileStorage& fs);

	void activation();
	Mat image();
	/*void sauvegarde();
	void calcBoardCornerPositions(Size boardSize, float squareSize, vector<Point3f>& corners);
	bool calibrationExtrinsec(vector<vector<Point2f> > imagePoints, Size boardSize);
	*/
public:
	int cameraID;
	Mat cameraMatrix;
	Mat distcoefs;
	Mat rvecs;
	Mat tvecs;
	Mat temp;
	VideoCapture inputCapture;
	bool goodInput;
	string outputFile;
};

#endif
