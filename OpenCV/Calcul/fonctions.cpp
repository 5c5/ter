#include <iostream>
#include <stdlib.h>
#include <sstream>
#include <time.h>
#include <stdio.h>
#include <X11/Xlib.h>
#include <pthread.h>

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/nonfree/nonfree.hpp"
#include "opencv2/nonfree/features2d.hpp"
#include "calcul.hpp"

using namespace cv;
using namespace std;

/*
 * Fonctions générales
 */
void usage(){
	
	printf("Analyse de projecteur : \n Usage : analyse Fichier1 [Fichier2] [Fichier3]\n Les fichiers doivent être au format .xml ou .yml\n\n");
}

/*
 * Fonction pour la verification de la présence de la mire
 */
bool mire(Mat& img, Size boardSize, vector<Point2f>& pointBuf){
	
	bool found = false;
	found = findChessboardCorners( img, boardSize, pointBuf );//, CV_CALIB_CB_ADAPTIVE_THRESH | CV_CALIB_CB_FAST_CHECK | CV_CALIB_CB_NORMALIZE_IMAGE);
	if ( found){
		Mat viewGray;
		cvtColor(img, viewGray, COLOR_BGR2GRAY);
		cornerSubPix( viewGray, pointBuf, Size(11,11), Size(-1,-1), TermCriteria( CV_TERMCRIT_EPS+CV_TERMCRIT_ITER, 30, 0.1 ));
	}
	return found;
}

