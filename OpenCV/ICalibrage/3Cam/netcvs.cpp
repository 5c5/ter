#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <iostream>
#include <string>
#include <pthread.h>

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/highgui/highgui.hpp>

using namespace std;
using namespace cv;
                        
Mat     img;
int     is_data_ready = 0;
int     listenSock, connectSock;
int 	listenPort;

Size boardSize;

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

void* streamServer(void* arg);
void  quit(string msg, int retval);

bool runCalibrationAndSave(Size imageSize, vector<vector<Point2f> > imagePoints );


int main(int argc, char** argv){
	
	pthread_t thread_s;
	int width, height, compteur = 0;
	vector<vector<Point2f> > imagePoints;
	Size imgSize;
	char key;
    clock_t prevTimestamp = 0;
	Mat view;
	width = 640;  
	height = 480;
	boardSize.width = 7;
	boardSize.height = 4;

		if (argc != 2) {
		quit("Usage: netcv_server <listen_port> ", 0);
	}
	
	listenPort = atoi(argv[1]);
 	
    img = Mat::zeros( height,width, CV_8UC1);
        
	/* run the streaming server as a separate thread */
	if (pthread_create(&thread_s, NULL, streamServer, NULL)) {
			quit("pthread_create failed.", 1);
	}

	cout << "\n-->Press 'q' to quit." << endl;
	namedWindow("stream_server");

	while(key != 'q') {

        vector<Point2f> pointBuf;
        bool found  = false;

		pthread_mutex_lock(&mutex);
		
		if (is_data_ready) {
			
			view = img;
			found = findChessboardCorners( view, boardSize, pointBuf, CV_CALIB_CB_ADAPTIVE_THRESH | CV_CALIB_CB_FAST_CHECK | CV_CALIB_CB_NORMALIZE_IMAGE);	
			drawChessboardCorners( img, boardSize, Mat(pointBuf), found );
			imshow("stream_server", img);
			is_data_ready = 0;
		}
		
		pthread_mutex_unlock(&mutex);
		if(found){
			
			cornerSubPix( view, pointBuf, Size(11,11), Size(-1,-1), TermCriteria( CV_TERMCRIT_EPS+CV_TERMCRIT_ITER, 30, 0.1 ));
			if( clock() - prevTimestamp > 500*1e-3*CLOCKS_PER_SEC){
				
				imagePoints.push_back(pointBuf);
				prevTimestamp = clock();
				compteur++;
			}

 
		}
		key = (char) waitKey(10);
		if(compteur > 24){
			key ='q';
			printf("Calibration finie\n");
			runCalibrationAndSave(view.size(), imagePoints);
		}
	}

	if (pthread_cancel(thread_s)) {
		quit("pthread_cancel failed.", 1);
	}

	destroyWindow("stream_server");
	quit("NULL", 0);
	return 0;
}

/////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * This is the streaming server, run as separate thread
 */
void* streamServer(void* arg){

	struct  sockaddr_in   serverAddr,  clientAddr;
	socklen_t             clientAddrLen = sizeof(clientAddr);

	/* make this thread cancellable using pthread_cancel() */
	pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
	pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);

	if ((listenSock = socket(PF_INET, SOCK_STREAM, 0)) < 0) {
		quit("socket() failed.", 1);
	}
		
	serverAddr.sin_family = PF_INET;
	serverAddr.sin_addr.s_addr = htonl(INADDR_ANY);
	serverAddr.sin_port = htons(listenPort);

	if (bind(listenSock, (sockaddr*)&serverAddr, sizeof(serverAddr)) == -1) {
			quit("bind() failed", 1);
	}
			
	if (listen(listenSock, 5) == -1) {
			quit("listen() failed.", 1);
	}
	
	int  imgSize = img.total()*img.elemSize();
	char sockData[imgSize];
	int  bytes=0;
			
	/* start receiving images */
	while(1){
		
		cout << "-->Waiting for TCP connection on port " << listenPort << " ...\n\n";
	
		/* accept a request from a client */
		if ((connectSock = accept(listenSock, (sockaddr*)&clientAddr, &clientAddrLen)) == -1) {
			quit("accept() failed", 1);
		}else{
			cout << "-->Receiving image from " << inet_ntoa(clientAddr.sin_addr) << ":" << ntohs(clientAddr.sin_port) << "..." << endl;
		}
		
		memset(sockData, 0x0, sizeof(sockData));
		
		while(1){

			for (int i = 0; i < imgSize; i += bytes) {
				if ((bytes = recv(connectSock, sockData +i, imgSize  - i, 0)) == -1) {
					
					quit("recv failed", 1);
				}
			}
			/* convert the received data to OpenCV's Mat format, thread safe */
			pthread_mutex_lock(&mutex);
			for (int i = 0;  i < img.rows; i++) {
				for (int j = 0; j < img.cols; j++) {
						(img.row(i)).col(j) = (uchar)sockData[((img.cols)*i)+j];
				}
			}
			is_data_ready = 1;
			memset(sockData, 0x0, sizeof(sockData));
			pthread_mutex_unlock(&mutex);
		}
	}

	/* have we terminated yet? */
	pthread_testcancel();

	/* no, take a rest for a while */
	usleep(1000);
	
}
/////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * This function provides a way to exit nicely from the system
 */
void quit(string msg, int retval)
{
        if (retval == 0) {
                cout << (msg == "NULL" ? "" : msg) << "\n" <<endl;
        } else {
                cerr << (msg == "NULL" ? "" : msg) << "\n" <<endl;
        }
         
        if (listenSock){
                close(listenSock);
        }

        if (connectSock){
                close(connectSock);
        }
                                
        if (!img.empty()){
                (img.release());
        }
                
        pthread_mutex_destroy(&mutex);
        exit(retval);
}

/*
 * Fonction de calcul des emplcaments des coins de la mire
 * */
static void calcBoardCornerPositions(Size boardSize, float squareSize, vector<Point3f>& corners){
    corners.clear();
	for( int i = 0; i < boardSize.height; ++i )
		for( int j = 0; j < boardSize.width; ++j )
			corners.push_back(Point3f(float( j*squareSize ), float( i*squareSize ), 0));
}


/*
 * Fonction de sauvegarde des paramètres
 * */
static void saveCameraParams(Mat& cameraMatrix, Mat& distCoeffs, const vector<Mat>& rvecs, const vector<Mat>& tvecs, const vector<vector<Point2f> >& imagePoints){
    FileStorage fs( "intrinsc3.yml", FileStorage::WRITE );
    time_t tm;
    time( &tm );
    struct tm *t2 = localtime( &tm );
    char buf[1024];
    strftime( buf, sizeof(buf)-1, "%c", t2 );

	fs << "Numero_Camera" << 2;
    fs << "Camera_Matrix" << cameraMatrix;
    fs << "Distortion_Coefficients" << distCoeffs;

	if( !rvecs.empty() && !tvecs.empty() ){

        CV_Assert(rvecs[0].type() == tvecs[0].type());
        Mat bigmat((int)rvecs.size(), 6, rvecs[0].type());
        for( int i = 0; i < (int)rvecs.size(); i++ )
        {
            Mat r = bigmat(Range(i, i+1), Range(0,3));
            Mat t = bigmat(Range(i, i+1), Range(3,6));

            CV_Assert(rvecs[i].rows == 3 && rvecs[i].cols == 1);
            CV_Assert(tvecs[i].rows == 3 && tvecs[i].cols == 1);
            //*.t() is MatExpr (not Mat) so we can use assignment operator
            r = rvecs[i].t();
            t = tvecs[i].t();
        }
        cvWriteComment( *fs, "a set of 6-tuples (rotation vector + translation vector) for each view", 0 );
        fs << "Extrinsic_Parameters" << bigmat;
    }

    if( !imagePoints.empty() )
    {
        Mat imagePtMat((int)imagePoints.size(), (int)imagePoints[0].size(), CV_32FC2);
        for( int i = 0; i < (int)imagePoints.size(); i++ )
        {
            Mat r = imagePtMat.row(i).reshape(2, imagePtMat.cols);
            Mat imgpti(imagePoints[i]);
            imgpti.copyTo(r);
        }
        fs << "Image_points" << imagePtMat;
    }
}



/*
 * Fonction de calibration
 * */
static bool runCalibration(Size& imageSize, Mat& cameraMatrix, Mat& distCoeffs, vector<vector<Point2f> > imagePoints, vector<Mat>& rvecs, vector<Mat>& tvecs){

    cameraMatrix = Mat::eye(3, 3, CV_64F);
	cameraMatrix.at<double>(0,0) = 1.0;

    distCoeffs = Mat::zeros(8, 1, CV_64F);

    vector<vector<Point3f> > objectPoints(1);
    calcBoardCornerPositions(boardSize, 1, objectPoints[0]);

    objectPoints.resize(imagePoints.size(),objectPoints[0]);

    //Find intrinsic and extrinsic camera parameters
    double rms = calibrateCamera(objectPoints, imagePoints, imageSize, cameraMatrix,distCoeffs, rvecs, tvecs, CV_CALIB_FIX_K4|CV_CALIB_FIX_K5);

    cout << "Re-projection error reported by calibrateCamera: "<< rms << endl;

    bool ok = checkRange(cameraMatrix) && checkRange(distCoeffs);

    return ok;
}


bool runCalibrationAndSave(Size imageSize, vector<vector<Point2f> > imagePoints ){
	
	Mat cameraMatrix, distCoeffs;
    vector<Mat> rvecs, tvecs;
    vector<float> reprojErrs;
    double totalAvgErr = 0;

    bool ok = runCalibration(imageSize, cameraMatrix, distCoeffs, imagePoints, rvecs, tvecs);
    cout << (ok ? "Calibration succeeded" : "Calibration failed")
        << ". avg re projection error = "  << totalAvgErr ;

    if( ok )
        saveCameraParams(cameraMatrix, distCoeffs, rvecs ,tvecs, imagePoints);
    return ok;
}
