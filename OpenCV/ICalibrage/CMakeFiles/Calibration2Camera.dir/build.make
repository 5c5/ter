# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 2.8

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list

# Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# The program to use to edit the cache.
CMAKE_EDIT_COMMAND = /usr/bin/ccmake

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /Students/p1410822/TER/OpenCV/Calibrage

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /Students/p1410822/TER/OpenCV/Calibrage

# Include any dependencies generated for this target.
include CMakeFiles/Calibration2Camera.dir/depend.make

# Include the progress variables for this target.
include CMakeFiles/Calibration2Camera.dir/progress.make

# Include the compile flags for this target's objects.
include CMakeFiles/Calibration2Camera.dir/flags.make

CMakeFiles/Calibration2Camera.dir/2camera_calibration.cpp.o: CMakeFiles/Calibration2Camera.dir/flags.make
CMakeFiles/Calibration2Camera.dir/2camera_calibration.cpp.o: 2camera_calibration.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report /Students/p1410822/TER/OpenCV/Calibrage/CMakeFiles $(CMAKE_PROGRESS_1)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object CMakeFiles/Calibration2Camera.dir/2camera_calibration.cpp.o"
	/usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/Calibration2Camera.dir/2camera_calibration.cpp.o -c /Students/p1410822/TER/OpenCV/Calibrage/2camera_calibration.cpp

CMakeFiles/Calibration2Camera.dir/2camera_calibration.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/Calibration2Camera.dir/2camera_calibration.cpp.i"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /Students/p1410822/TER/OpenCV/Calibrage/2camera_calibration.cpp > CMakeFiles/Calibration2Camera.dir/2camera_calibration.cpp.i

CMakeFiles/Calibration2Camera.dir/2camera_calibration.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/Calibration2Camera.dir/2camera_calibration.cpp.s"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /Students/p1410822/TER/OpenCV/Calibrage/2camera_calibration.cpp -o CMakeFiles/Calibration2Camera.dir/2camera_calibration.cpp.s

CMakeFiles/Calibration2Camera.dir/2camera_calibration.cpp.o.requires:
.PHONY : CMakeFiles/Calibration2Camera.dir/2camera_calibration.cpp.o.requires

CMakeFiles/Calibration2Camera.dir/2camera_calibration.cpp.o.provides: CMakeFiles/Calibration2Camera.dir/2camera_calibration.cpp.o.requires
	$(MAKE) -f CMakeFiles/Calibration2Camera.dir/build.make CMakeFiles/Calibration2Camera.dir/2camera_calibration.cpp.o.provides.build
.PHONY : CMakeFiles/Calibration2Camera.dir/2camera_calibration.cpp.o.provides

CMakeFiles/Calibration2Camera.dir/2camera_calibration.cpp.o.provides.build: CMakeFiles/Calibration2Camera.dir/2camera_calibration.cpp.o

# Object files for target Calibration2Camera
Calibration2Camera_OBJECTS = \
"CMakeFiles/Calibration2Camera.dir/2camera_calibration.cpp.o"

# External object files for target Calibration2Camera
Calibration2Camera_EXTERNAL_OBJECTS =

Calibration2Camera: CMakeFiles/Calibration2Camera.dir/2camera_calibration.cpp.o
Calibration2Camera: CMakeFiles/Calibration2Camera.dir/build.make
Calibration2Camera: CMakeFiles/Calibration2Camera.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --red --bold "Linking CXX executable Calibration2Camera"
	$(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/Calibration2Camera.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
CMakeFiles/Calibration2Camera.dir/build: Calibration2Camera
.PHONY : CMakeFiles/Calibration2Camera.dir/build

CMakeFiles/Calibration2Camera.dir/requires: CMakeFiles/Calibration2Camera.dir/2camera_calibration.cpp.o.requires
.PHONY : CMakeFiles/Calibration2Camera.dir/requires

CMakeFiles/Calibration2Camera.dir/clean:
	$(CMAKE_COMMAND) -P CMakeFiles/Calibration2Camera.dir/cmake_clean.cmake
.PHONY : CMakeFiles/Calibration2Camera.dir/clean

CMakeFiles/Calibration2Camera.dir/depend:
	cd /Students/p1410822/TER/OpenCV/Calibrage && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /Students/p1410822/TER/OpenCV/Calibrage /Students/p1410822/TER/OpenCV/Calibrage /Students/p1410822/TER/OpenCV/Calibrage /Students/p1410822/TER/OpenCV/Calibrage /Students/p1410822/TER/OpenCV/Calibrage/CMakeFiles/Calibration2Camera.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : CMakeFiles/Calibration2Camera.dir/depend

